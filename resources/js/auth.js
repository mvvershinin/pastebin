import bearer from '@websanova/vue-auth/drivers/auth/bearer'
import axios from '@websanova/vue-auth/drivers/http/axios.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'
// Auth base configuration some of this options
// can be override in method calls
const config = {
    auth: bearer,
    http: axios,
    router: router,
    tokenDefaultName: 'Authorization',
    tokenStore: ['localStorage'],
    rolesVar: 'role',
    //registerData: {url: 'auth/register', method: 'POST', redirect: '/login'},
    loginData: {url: 'login', method: 'POST', redirect: '/create', fetchUser: true},
    logoutData: {url: 'logout', method: 'POST', redirect: '/create', makeRequest: true},
    fetchData: {url: 'user', method: 'GET', enabled: true},
    //refreshData: {url: 'auth/refresh', method: 'GET', enabled: true, interval: 30}
    googleOauth2Data: {
        url: 'https://accounts.google.com/o/oauth2/auth',
        params: {
            redirect_uri: function () {
                    return this.options.getUrl() + '/login/google';
                },
            // TODO set google id
            client_id: '357864430677-0ldtretrbsln6b3fi9pd52b0t0i8k3js.apps.googleusercontent.com',
            scope: 'email profile'
        }
    }
}
export default config
