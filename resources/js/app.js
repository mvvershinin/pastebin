import 'es6-promise/auto'
import axios from 'axios'
import './bootstrap'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'


import App from './views/App'
import auth from './auth'
import router from './routes'

// Set Vue globally
window.Vue = Vue;

// Set Vue router
Vue.router = router;
Vue.use(VueRouter);


// Set Vue authentication
Vue.use(VueAxios, axios);

//axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`
axios.defaults.baseURL = '/api';

//Auth
Vue.use(VueAuth, auth);

// Social Auth


// prism code highlight
import "prismjs";
import "prismjs/themes/prism-tomorrow.css";
import "prismjs/components/prism-scss.min";
import "prismjs/plugins/autolinker/prism-autolinker.min";
import "prismjs/plugins/autolinker/prism-autolinker.css";
import Prism from "vue-prism-component";
Vue.component("prism", Prism);

//paginator
Vue.component('pagination', require('laravel-vue-pagination'));

// Load Index
Vue.component('app', App);



const app = new Vue({
    el: '#app',
    router
});
