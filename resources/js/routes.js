import VueRouter from 'vue-router'
// Pages
import Login from './views/auth/Login'
import Show from './views/Show'
import Create from './views/Create'
import Search from './views/Search'
import Cabinet from './views/Cabinet'

// Routes
const routes = [
    {
        path: '/',
        redirect: '/create'
    },
    {
        path: '/create',
        name: 'create',
        component: Create,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/search',
        name: 'search',
        component: Search,
        meta: {
            auth: undefined
        }
    },
    {
        path: '/cabinet',
        name: 'cabinet',
        component: Cabinet,
        meta: {
            auth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/logout',
        name: 'logout',
        component: Login,
        meta: {
            auth: true
        }
    },
    {
        path: '/login/:type',
        name: 'oauth2-type',
        component: Login,
        meta: {
            auth: false
        }
    },
    {
        path: '/:id',
        name: 'show',
        component: Show,
        props: true,
        meta: {
            auth: undefined
        }
    },
];

const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
});

export default router
