<?php

namespace App\Models;

use App\Http\Traits\Filterable;
use App\Scopes\ExpiredScope;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;


class Paste extends Model
{
    use Sluggable, Filterable;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ExpiredScope());
    }

    protected $fillable = [
        'user_id',
        'hash',
        'name',
        'text',
        'expiration_time',
        'access'
    ];

    protected $with = 'user';

    protected $dates = ['expiration_time'];

    public function sluggable()
    {
        return [
            'hash' => [
                'source' => ['hash_source'],
                'onUpdate' => false,
            ]
        ];
    }

    /**
     * make hash
     *
     * @return string
     */
    public function getHashSourceAttribute()
    {
        return hash('md5', $this->name . now());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
