<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(LoginRequest $request)
    {
        $credentials = request(['name', 'password']);

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('personal');
        $token = $tokenResult->token;

        $token->save();

        return response()->json(['status' => 'success'], 200)->header('Authorization', $tokenResult->accessToken);
    }

    /**
     * dont used at frontend
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $request['password'] = Hash::make($request['password']);
        $request['role'] = 'user';

        $user = User::create($request->toArray());
        $tokenResult = $user->createToken('personal');

        return response()->json(['status' => 'success'], 200)->header('Authorization', $tokenResult->accessToken);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * login with google
     *
     * create user or login from google account
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function google(Request $request)
    {
        $token = Socialite::with('google')->getAccessTokenResponse($request->code);
        $user = Socialite::with('google')->userFromToken($token['access_token']);
        $exist = User::where('name', $user->email)->orWhere('email', $user->email)->first();

        if (empty($exist)) {
            $new['name'] = $user->email;
            $new['email'] = $user->email;
            $new['password'] = Hash::make(now());
            $new['role'] = 'user';
            $user = User::create($new);
        } else {
            $user = $exist;
        }

        $tokenResult = $user->createToken('personal');

        return response()->json(['status' => 'success'], 200)->header('Authorization', $tokenResult->accessToken);
    }

    public function user()
    {
        return response()->json([
            'status' => 'success',
            'data' => Auth::user()
        ]);
    }
}
