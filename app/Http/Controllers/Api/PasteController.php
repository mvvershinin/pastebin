<?php

namespace App\Http\Controllers\Api;

use App\Http\Middleware\Authenticate;
use App\Http\Requests\StorePasteRequest;
use App\Models\Paste;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PasteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json([
            'success' => Paste::search($request)->filter($request)->paginate(10)
        ], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePasteRequest $request)
    {
        $data = $request->toArray();

        $data['text'] = trim($data['text']);
        $data['expiration_time'] = getTime($data['expiration_time']);

        if ($request->user('api')) {
            $data['user_id'] = $request->user('api')->id;
        }

        $new = Paste::create($data);

        return response()->json(['success' => $new->toArray()], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Paste $paste
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $paste)
    {
        return response()->json([
            'success' => Paste::access($request)->where('hash', $paste)->firstOrFail()
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Paste $paste
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paste $paste)
    {
        return response()->json(['message' => 'not supported', 'error' => true], 200);
    }

    /**
     * Delete not supported.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        return response()->json(['message' => 'not supported', 'error' => true], 200);
    }

}
