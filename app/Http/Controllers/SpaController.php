<?php

namespace App\Http\Controllers;

use App\Models\Paste;
use App\Models\User;
use \App\Providers\UsedCarsServiceProvider;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class SpaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app');
    }
}
