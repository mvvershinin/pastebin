<?php


namespace App\Http\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

trait Filterable
{
    /**
     * params for query current orders
     *
     * @param Builder $query
     * @param String $search
     * @return Builder
     */
    public function scopeAccess(Builder $query, Request $request)
    {
        if (empty($request->user('api'))) {
            return $query->whereIn('access', [ACCESS_PUBLIC, ACCESS_UNLISTED]);
        } else {

            return $query
                ->whereHas('user',
                    function ($builder) use ($request){
                        $builder->where('id', $request->user('api')->id);
                    })
                ->orWhereNotIn('access', [ACCESS_PRIVATE]);
        }
    }
    /**
     * params for query current orders
     *
     * @param Builder $query
     * @param String $search
     * @return Builder
     */
    public function scopeSearch(Builder $query, Request $request)
    {
        $search = $request->get('search');
        return
            $query
                ->when($search, function ($builder) use ($search) {
                    $builder
                        ->where('name', 'like', "%$search%")
                        ->orWhere('text', 'like', "%$search%");
                });
    }

    /**
     * get filter by request
     *
     * if not loget in - just all public
     * if logget in and query has own - all user's access types
     * if logget in and query not own - all public
     *
     * @param Builder $query
     * @param Request $request
     * @return Builder|mixed
     */
    public function scopeFilter(Builder $query, Request $request)
    {
        if (empty($request->user('api'))) {
            return $query->where('access', ACCESS_PUBLIC);
        } else {
            return $query
                ->when(($request->get('own') == 'false') || empty($request->get('own')), function ($builder) {
                    $builder
                        ->where('access', ACCESS_PUBLIC);
                })
                ->when($request->get('own') == 'true', function ($builder) use ($request) {
                    $builder
                        ->where('user_id', $request->user('api')->id);
                });
        }
    }
}
