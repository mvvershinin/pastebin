<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\{Scope, Model, Builder};

class ExpiredScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder
            ->where('expiration_time', '>', now('0'))
            ->latest('created_at');
    }
}
