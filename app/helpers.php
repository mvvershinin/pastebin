<?php

function getTime($time)
{
    switch ($time) {
        case 'min10':
            return now()->addMinutes(10);
            break;
        case 'hour1':
            return now()->addHour();
            break;
        case 'hour3':
            return now()->addHours(3);
            break;
        case 'day1':
            return now()->addDay();
            break;
        case 'week1':
            return now()->addWeek();
            break;
        case 'month1':
            return now()->addMonth();
            break;
        case 'nolimit':
            return now()->addCenturies(1);
            break;
        default:
            return now()->addDay();
    }
}
