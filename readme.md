## How to install

- git clone --recursive https://bitbucket.org/mvvershinin/pastebin.git
- cd pastebin/laradock/
- cp .env.example .env
 ###if need change data store for containers from 
    DATA_PATH_HOST=~/.laradock/paste_folder/data
- cd nginx/sites 
- cp paste.conf.example paste.conf
###change servername in paste.conf to your domain
    example: server_name paste.nyanyanya.site
-cd ../..
- sudo docker-compose build
- sudo docker-compose up -d

###check - need looks like this

- sudo docker-compose ps

###if mysql dont start try 
    rename laradock/mysql/docker-entrypoint-initdb.d/create.sql.example to create.sql

###open container console
- sudo docker-compose exec -u laradock workspace bash

- cp .env.example .env
###enter 
    google_id & google_secret in .env
    in file /resources/js/auth.js
    googl_id ->    client_id: '357864430677-0ldtretrbsln6b3fi9pd52b0t0i8k3js.apps.googleusercontent.com', 

- php artisan key:generate

- php artisan migrate

- php artisan passport:install

- php artisan passport:client --personal

- php artisan db:seed


###create frontend

- npm install
- npm run production


[http://paste.nyanyanya.site]
if not online - i can just on it in all time - it hosted on home pc :)

### users for test
    user:user_password
    
    admin:admin_password
